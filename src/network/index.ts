// Libraries
import PubSub from 'pubsub-js';

// Utils
import {
  PacketType,
  Packet,
  AuthPacket,
  CharacterSelectedPacket,
  RemoteFacingPacket,
  RemoteVelocityPacket,
  KeepAlivePacket,
} from './Packet';

export enum NetworkStatus {
  Connected = 'connected',
  Disconnected = 'disconnected',
}

export interface NetworkChangeContext {
  status: NetworkStatus;
  code?: number;
  reason?: string;
}

export class NetworkModule {
  private _ws?: WebSocket;
  private _userId?: string;
  private _charUID?: number;

  private _connectToken: string;
  private _charSelToken: string = '';

  constructor() {
    // Listen for the command to establish a websocket to the server
    this._connectToken = PubSub.subscribe(
      'network.connect',
      this._connect.bind(this)
    );

    // Listen for when the character is selected (from UI)
    this._charSelToken = PubSub.subscribe(
      'character.selected',
      this._handleCharacterSelected.bind(this)
    );
  }

  // This is called when the player is ready to enter the game
  // world (after login and character selection)
  private _connect() {
    PubSub.unsubscribe(this._connectToken);

    this._ws = new WebSocket('ws://localhost:8080');
    this._ws.binaryType = 'arraybuffer';
    this._ws.addEventListener('open', this._onOpen.bind(this));
    this._ws.addEventListener('close', this._onClose.bind(this));
    this._ws.addEventListener('message', this._onMessage.bind(this));
  }

  public send(packet: Packet) {
    this._ws!.send(packet.serialize());
  }

  private _onOpen() {
    // Start listening for other modules trying to send data
    PubSub.subscribe('network.send', (_, packet: Packet) => {
      this.send(packet);
    });

    // Socket is open, send token
    const token = sessionStorage.getItem('access_token') ?? '';

    const packet = new AuthPacket(token);
    this.send(packet);
  }

  private _onClose(e: CloseEvent) {
    // Log that the server disconnected
    console.log(
      `Server closed socket with code ${e.code} and reason: ${
        e.reason || 'No reason'
      }`
    );

    // Publish the network status so other systems can react to it
    PubSub.publish('network.status', {
      status: NetworkStatus.Disconnected,
      code: e.code,
      reason: e.reason,
    });

    // Clear our session token since we'll have to re-login
    sessionStorage.removeItem('access_token');

    // Start listening for new connection requests
    this._connectToken = PubSub.subscribe(
      'network.connect',
      this._connect.bind(this)
    );
  }

  private _onMessage(e: MessageEvent) {
    const type = new DataView(e.data).getUint8(0);

    if (type === PacketType.AuthSuccess) {
      this._handleAuthSuccess();
    } else if (type === PacketType.CharacterSelected) {
      const packet = CharacterSelectedPacket.parse(e.data);
      PubSub.publish('network.account.character_selected', packet.uid);
    } else if (type === PacketType.RemoteFacing) {
      const packet = RemoteFacingPacket.parse(e.data);
      if (packet.uid === this._charUID) {
        PubSub.publish('network.local.correction.facing', packet.facing);
      } else {
        PubSub.publish(`network.remote.${packet.uid}.facing`, packet.facing);
      }
    } else if (type === PacketType.RemoteVelocity) {
      const packet = RemoteVelocityPacket.parse(e.data);
      if (packet.uid === this._charUID) {
        PubSub.publish('network.local.correction.position', packet.position);
      } else {
        PubSub.publish(`network.remote.${packet.uid}.velocity`, {
          velocity: packet.velocity,
          position: packet.position,
        });
      }
    } else if (type === PacketType.KeepAlive) {
      // If we get a keep alive packet, immediately respond to the server
      this.send(new KeepAlivePacket(false));
    }
  }

  private _handleAuthSuccess() {
    // Successfully authenticated
    PubSub.publish('network.account.authenticated');

    // Parse the user id from JWT and store it
    const token = sessionStorage.getItem('access_token') ?? '';
    this._userId = JSON.parse(atob(token.split('.')[1])).sub;

    // Setup authenticated listeners
    this._setupAuthedListeners();
  }

  private _setupAuthedListeners() {}

  private _handleCharacterSelected(_: string, charData: any) {
    this._charUID = charData.uid;
  }
}
