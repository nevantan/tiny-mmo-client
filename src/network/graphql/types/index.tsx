import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type BarValue = {
  __typename?: 'BarValue';
  current: Scalars['Int'];
  total: Scalars['Int'];
};

export type Character = {
  __typename?: 'Character';
  id: Scalars['ID'];
  name: Scalars['String'];
  player?: Maybe<User>;
  position: Vector2;
  tag: Scalars['Int'];
  taggedName: Scalars['String'];
  uid: Scalars['Int'];
  zone: Scalars['String'];
};

export type Mob = {
  __typename?: 'Mob';
  id: Scalars['ID'];
  name: Scalars['String'];
  position: Vector2;
  type: MobType;
  zone: Scalars['String'];
};

export enum MobType {
  GreenSlime = 'GreenSlime'
}

export type Mutation = {
  __typename?: 'Mutation';
  login: Scalars['String'];
  selectCharacter: Character;
};


export type MutationLoginArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationSelectCharacterArgs = {
  uid: Scalars['Int'];
};

export type Query = {
  __typename?: 'Query';
  character?: Maybe<Character>;
  characters: Array<Maybe<Character>>;
  user?: Maybe<User>;
  zone?: Maybe<Zone>;
};


export type QueryCharacterArgs = {
  uid: Scalars['Int'];
};


export type QueryUserArgs = {
  id: Scalars['ID'];
};


export type QueryZoneArgs = {
  id: Scalars['ID'];
};

export type Subscription = {
  __typename?: 'Subscription';
  mobSpawn: Mob;
  remoteJoined: Character;
  remoteLeft: Scalars['Int'];
};

export type User = {
  __typename?: 'User';
  characters: Array<Maybe<Character>>;
  email: Scalars['String'];
  id: Scalars['ID'];
};

export type Vector2 = {
  __typename?: 'Vector2';
  x: Scalars['Int'];
  y: Scalars['Int'];
};

export type Zone = {
  __typename?: 'Zone';
  characters: Array<Maybe<Character>>;
  id: Scalars['ID'];
  mobs: Array<Maybe<Mob>>;
};

export type GetCharacterQueryVariables = Exact<{
  uid: Scalars['Int'];
}>;


export type GetCharacterQuery = { __typename?: 'Query', character?: { __typename?: 'Character', id: string, uid: number, name: string, tag: number, taggedName: string, position: { __typename?: 'Vector2', x: number, y: number } } | null };

export type RemoteJoinSubscriptionVariables = Exact<{ [key: string]: never; }>;


export type RemoteJoinSubscription = { __typename?: 'Subscription', remoteJoined: { __typename?: 'Character', uid: number, name: string, tag: number, taggedName: string, position: { __typename?: 'Vector2', x: number, y: number } } };

export type RemoteLeaveSubscriptionVariables = Exact<{ [key: string]: never; }>;


export type RemoteLeaveSubscription = { __typename?: 'Subscription', remoteLeft: number };

export type GetZoneDataQueryVariables = Exact<{
  id: Scalars['ID'];
}>;


export type GetZoneDataQuery = { __typename?: 'Query', zone?: { __typename?: 'Zone', id: string, characters: Array<{ __typename?: 'Character', uid: number, name: string, tag: number, taggedName: string, position: { __typename?: 'Vector2', x: number, y: number } } | null>, mobs: Array<{ __typename?: 'Mob', id: string, type: MobType, name: string, position: { __typename?: 'Vector2', x: number, y: number } } | null> } | null };

export type GetCharactersQueryVariables = Exact<{ [key: string]: never; }>;


export type GetCharactersQuery = { __typename?: 'Query', characters: Array<{ __typename?: 'Character', uid: number, taggedName: string } | null> };

export type SelectCharacterMutationVariables = Exact<{
  uid: Scalars['Int'];
}>;


export type SelectCharacterMutation = { __typename?: 'Mutation', selectCharacter: { __typename?: 'Character', id: string, uid: number, name: string, tag: number, taggedName: string, zone: string, position: { __typename?: 'Vector2', x: number, y: number } } };

export type LoginMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = { __typename?: 'Mutation', login: string };


export const GetCharacterDocument = gql`
    query GetCharacter($uid: Int!) {
  character(uid: $uid) {
    id
    uid
    name
    tag
    taggedName
    position {
      x
      y
    }
  }
}
    `;

/**
 * __useGetCharacterQuery__
 *
 * To run a query within a React component, call `useGetCharacterQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCharacterQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCharacterQuery({
 *   variables: {
 *      uid: // value for 'uid'
 *   },
 * });
 */
export function useGetCharacterQuery(baseOptions: Apollo.QueryHookOptions<GetCharacterQuery, GetCharacterQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCharacterQuery, GetCharacterQueryVariables>(GetCharacterDocument, options);
      }
export function useGetCharacterLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCharacterQuery, GetCharacterQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCharacterQuery, GetCharacterQueryVariables>(GetCharacterDocument, options);
        }
export type GetCharacterQueryHookResult = ReturnType<typeof useGetCharacterQuery>;
export type GetCharacterLazyQueryHookResult = ReturnType<typeof useGetCharacterLazyQuery>;
export type GetCharacterQueryResult = Apollo.QueryResult<GetCharacterQuery, GetCharacterQueryVariables>;
export const RemoteJoinDocument = gql`
    subscription RemoteJoin {
  remoteJoined {
    uid
    name
    tag
    taggedName
    position {
      x
      y
    }
  }
}
    `;

/**
 * __useRemoteJoinSubscription__
 *
 * To run a query within a React component, call `useRemoteJoinSubscription` and pass it any options that fit your needs.
 * When your component renders, `useRemoteJoinSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useRemoteJoinSubscription({
 *   variables: {
 *   },
 * });
 */
export function useRemoteJoinSubscription(baseOptions?: Apollo.SubscriptionHookOptions<RemoteJoinSubscription, RemoteJoinSubscriptionVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useSubscription<RemoteJoinSubscription, RemoteJoinSubscriptionVariables>(RemoteJoinDocument, options);
      }
export type RemoteJoinSubscriptionHookResult = ReturnType<typeof useRemoteJoinSubscription>;
export type RemoteJoinSubscriptionResult = Apollo.SubscriptionResult<RemoteJoinSubscription>;
export const RemoteLeaveDocument = gql`
    subscription RemoteLeave {
  remoteLeft
}
    `;

/**
 * __useRemoteLeaveSubscription__
 *
 * To run a query within a React component, call `useRemoteLeaveSubscription` and pass it any options that fit your needs.
 * When your component renders, `useRemoteLeaveSubscription` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the subscription, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useRemoteLeaveSubscription({
 *   variables: {
 *   },
 * });
 */
export function useRemoteLeaveSubscription(baseOptions?: Apollo.SubscriptionHookOptions<RemoteLeaveSubscription, RemoteLeaveSubscriptionVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useSubscription<RemoteLeaveSubscription, RemoteLeaveSubscriptionVariables>(RemoteLeaveDocument, options);
      }
export type RemoteLeaveSubscriptionHookResult = ReturnType<typeof useRemoteLeaveSubscription>;
export type RemoteLeaveSubscriptionResult = Apollo.SubscriptionResult<RemoteLeaveSubscription>;
export const GetZoneDataDocument = gql`
    query GetZoneData($id: ID!) {
  zone(id: $id) {
    id
    characters {
      uid
      name
      tag
      taggedName
      position {
        x
        y
      }
    }
    mobs {
      id
      type
      position {
        x
        y
      }
      name
    }
  }
}
    `;

/**
 * __useGetZoneDataQuery__
 *
 * To run a query within a React component, call `useGetZoneDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetZoneDataQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetZoneDataQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useGetZoneDataQuery(baseOptions: Apollo.QueryHookOptions<GetZoneDataQuery, GetZoneDataQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetZoneDataQuery, GetZoneDataQueryVariables>(GetZoneDataDocument, options);
      }
export function useGetZoneDataLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetZoneDataQuery, GetZoneDataQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetZoneDataQuery, GetZoneDataQueryVariables>(GetZoneDataDocument, options);
        }
export type GetZoneDataQueryHookResult = ReturnType<typeof useGetZoneDataQuery>;
export type GetZoneDataLazyQueryHookResult = ReturnType<typeof useGetZoneDataLazyQuery>;
export type GetZoneDataQueryResult = Apollo.QueryResult<GetZoneDataQuery, GetZoneDataQueryVariables>;
export const GetCharactersDocument = gql`
    query GetCharacters {
  characters {
    uid
    taggedName
  }
}
    `;

/**
 * __useGetCharactersQuery__
 *
 * To run a query within a React component, call `useGetCharactersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCharactersQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetCharactersQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetCharactersQuery(baseOptions?: Apollo.QueryHookOptions<GetCharactersQuery, GetCharactersQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<GetCharactersQuery, GetCharactersQueryVariables>(GetCharactersDocument, options);
      }
export function useGetCharactersLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<GetCharactersQuery, GetCharactersQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<GetCharactersQuery, GetCharactersQueryVariables>(GetCharactersDocument, options);
        }
export type GetCharactersQueryHookResult = ReturnType<typeof useGetCharactersQuery>;
export type GetCharactersLazyQueryHookResult = ReturnType<typeof useGetCharactersLazyQuery>;
export type GetCharactersQueryResult = Apollo.QueryResult<GetCharactersQuery, GetCharactersQueryVariables>;
export const SelectCharacterDocument = gql`
    mutation SelectCharacter($uid: Int!) {
  selectCharacter(uid: $uid) {
    id
    uid
    name
    tag
    taggedName
    zone
    position {
      x
      y
    }
  }
}
    `;
export type SelectCharacterMutationFn = Apollo.MutationFunction<SelectCharacterMutation, SelectCharacterMutationVariables>;

/**
 * __useSelectCharacterMutation__
 *
 * To run a mutation, you first call `useSelectCharacterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSelectCharacterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [selectCharacterMutation, { data, loading, error }] = useSelectCharacterMutation({
 *   variables: {
 *      uid: // value for 'uid'
 *   },
 * });
 */
export function useSelectCharacterMutation(baseOptions?: Apollo.MutationHookOptions<SelectCharacterMutation, SelectCharacterMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SelectCharacterMutation, SelectCharacterMutationVariables>(SelectCharacterDocument, options);
      }
export type SelectCharacterMutationHookResult = ReturnType<typeof useSelectCharacterMutation>;
export type SelectCharacterMutationResult = Apollo.MutationResult<SelectCharacterMutation>;
export type SelectCharacterMutationOptions = Apollo.BaseMutationOptions<SelectCharacterMutation, SelectCharacterMutationVariables>;
export const LoginDocument = gql`
    mutation Login($email: String!, $password: String!) {
  login(email: $email, password: $password)
}
    `;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<LoginMutation, LoginMutationVariables>;