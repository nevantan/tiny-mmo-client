// Libraries
import React from 'react';

// Modules
import { NetworkModule } from './';

interface NetworkProviderProps {
  children: React.ReactNode;
  network: NetworkModule;
}

const NetworkContext = React.createContext<NetworkModule | null>(null);

export const NetworkProvider: React.FC<NetworkProviderProps> = ({
  children,
  network,
}) => {
  return (
    <NetworkContext.Provider value={network}>
      {children}
    </NetworkContext.Provider>
  );
};
