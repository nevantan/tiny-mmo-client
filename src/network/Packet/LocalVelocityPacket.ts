import { Vector } from 'excalibur';
import { Packet } from './Packet';

// Types
import { PacketType } from './PacketType';

export class LocalVelocityPacket extends Packet {
  constructor(protected _velocity: Vector) {
    super(PacketType.LocalVelocity);
  }

  public serialize() {
    const packet = new ArrayBuffer(3);
    new DataView(packet).setUint8(0, this._type);
    new DataView(packet).setInt8(1, this._velocity.x);
    new DataView(packet).setInt8(2, this._velocity.y);

    return packet;
  }

  get velocity() {
    return this._velocity;
  }

  static parse(data: ArrayBuffer) {
    try {
      const x = new DataView(data).getInt8(0);
      const y = new DataView(data).getInt8(1);
      return new LocalVelocityPacket(new Vector(x, y));
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
