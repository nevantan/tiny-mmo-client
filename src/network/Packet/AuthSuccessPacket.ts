import { Packet } from './Packet';
import { PacketType } from './PacketType';

export class AuthSuccessPacket extends Packet {
  constructor() {
    super(PacketType.AuthSuccess);
  }

  public serialize() {
    const type = new Uint8Array([1]);

    return type;
  }

  static parse(data: ArrayBuffer) {
    try {
      return new AuthSuccessPacket();
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
