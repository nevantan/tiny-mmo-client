import { PacketType } from './PacketType';

export class Packet {
  constructor(protected _type: PacketType) {}

  public serialize() {
    return new ArrayBuffer();
  }

  get type() {
    return this._type;
  }

  static parse(data: Uint8Array) {
    const type = data[0];
    const payload = data.subarray(1);

    throw new Error(`Unsupported packet of type ${type}: ${payload}`);
  }
}
