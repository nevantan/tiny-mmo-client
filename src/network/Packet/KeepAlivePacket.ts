import { Packet } from './Packet';
import { PacketType } from './PacketType';

export class KeepAlivePacket extends Packet {
  constructor(protected _ping: boolean) {
    super(PacketType.KeepAlive);
  }

  public serialize() {
    const packet = new ArrayBuffer(2);
    new DataView(packet).setUint8(0, this._type);
    new DataView(packet).setUint8(1, this._ping ? 1 : 0);

    return packet;
  }

  get ping() {
    return this._ping;
  }

  static parse(data: ArrayBuffer) {
    try {
      return new KeepAlivePacket(
        new DataView(data).getUint8(1) === 1 ? true : false
      );
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
