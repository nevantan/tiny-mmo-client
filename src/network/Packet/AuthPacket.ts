import { Packet } from './Packet';
import { PacketType } from './PacketType';

export class AuthPacket extends Packet {
  constructor(protected _token: string) {
    super(PacketType.Auth);
  }

  public serialize() {
    const type = new Uint8Array([this._type]);
    const payload = new TextEncoder().encode(this._token);

    const merged = new Uint8Array(type.length + payload.length);
    merged.set(type);
    merged.set(payload, type.length);

    return merged;
  }

  get token() {
    return this._token;
  }

  static parse(data: Uint8Array) {
    try {
      return new AuthPacket(new TextDecoder().decode(data.subarray(1)));
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
