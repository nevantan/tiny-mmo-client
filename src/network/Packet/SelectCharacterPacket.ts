import { Packet } from './Packet';
import { PacketType } from './PacketType';

export class SelectCharacterPacket extends Packet {
  constructor(protected _characterUID: number) {
    super(PacketType.SelectCharacter);
  }

  public serialize() {
    const packet = new ArrayBuffer(5);
    new DataView(packet).setUint8(0, this._type);
    new DataView(packet).setInt32(1, this._characterUID, false);

    return packet;
  }

  get uid() {
    return this._characterUID;
  }

  static parse(data: ArrayBuffer) {
    try {
      return new SelectCharacterPacket(new DataView(data).getInt32(0, false));
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
