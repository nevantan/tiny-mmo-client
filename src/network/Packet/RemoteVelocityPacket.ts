// Libraries
import { Packet } from './Packet';
import { Vector } from 'excalibur';

// Types
import { PacketType } from './PacketType';

export class RemoteVelocityPacket extends Packet {
  constructor(
    protected _uid: number,
    protected _velocity: Vector,
    protected _position: Vector
  ) {
    super(PacketType.RemoteVelocity);
  }

  public serialize() {
    const packet = new ArrayBuffer(11);
    new DataView(packet).setUint8(0, this._type);
    new DataView(packet).setUint32(1, this._uid, false);
    new DataView(packet).setInt8(5, this._velocity.x);
    new DataView(packet).setInt8(6, this._velocity.y);
    new DataView(packet).setInt16(7, this._position.x, false);
    new DataView(packet).setInt16(9, this._position.y, false);

    return packet;
  }

  get uid() {
    return this._uid;
  }

  get velocity() {
    return this._velocity;
  }

  get position() {
    return this._position;
  }

  static parse(data: ArrayBuffer) {
    try {
      const uid = new DataView(data).getUint32(1, false);

      const vx = new DataView(data).getInt8(5);
      const vy = new DataView(data).getInt8(6);

      const px = new DataView(data).getInt16(7, false);
      const py = new DataView(data).getInt16(9, false);

      return new RemoteVelocityPacket(
        uid,
        new Vector(vx, vy),
        new Vector(px, py)
      );
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
