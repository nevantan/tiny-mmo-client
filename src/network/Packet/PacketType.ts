export enum PacketType {
  Auth = 0, // Client sending access_token to server
  AuthSuccess = 1, // Server acknowledging successful auth
  SelectCharacter = 2, // **Deprecated** Client indicating which character to select
  CharacterSelected = 3, // **Deprecated** Server acknowledging successful selection
  LocalFacing = 4, // Client notifying server of new facing direction
  LocalVelocity = 5, // Client notifying server of new velocity
  RemoteFacing = 6, // Server notifying clients of new facing direction
  RemoteVelocity = 7, // Server notifying clients of new velocity
  KeepAlive = 8,
}
