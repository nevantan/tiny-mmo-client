import { Packet } from './Packet';
import { PacketType } from './PacketType';

export class CharacterSelectedPacket extends Packet {
  constructor(protected _uid: number) {
    super(PacketType.CharacterSelected);
  }

  public serialize() {
    const packet = new ArrayBuffer(5);
    new DataView(packet).setUint8(0, this._type);
    new DataView(packet).setUint32(1, this._uid, false);

    return packet;
  }

  get uid() {
    return this._uid;
  }

  static parse(data: ArrayBuffer) {
    try {
      const uid = new DataView(data).getUint32(1, false);

      return new CharacterSelectedPacket(uid);
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
