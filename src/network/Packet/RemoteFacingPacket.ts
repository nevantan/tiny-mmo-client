import { Packet } from './Packet';

// Types
import { PacketType } from './PacketType';
import { Facing } from '../../enums/Direction';

export class RemoteFacingPacket extends Packet {
  constructor(protected _facing: Facing, protected _uid: number) {
    super(PacketType.RemoteFacing);
  }

  public serialize() {
    const packet = new ArrayBuffer(7);
    new DataView(packet).setUint8(0, this._type);
    new DataView(packet).setInt8(1, this._facing.x);
    new DataView(packet).setInt8(2, this._facing.y);
    new DataView(packet).setUint32(3, this._uid, false);

    return packet;
  }

  get facing() {
    return this._facing;
  }

  get uid() {
    return this._uid;
  }

  static parse(data: ArrayBuffer) {
    try {
      return new RemoteFacingPacket(
        {
          x: new DataView(data).getInt8(1),
          y: new DataView(data).getInt8(2),
        },
        new DataView(data).getUint32(3, false)
      );
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
