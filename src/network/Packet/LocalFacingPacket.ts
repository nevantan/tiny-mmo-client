import { Packet } from './Packet';

// Types
import { PacketType } from './PacketType';
import { Facing } from '../../enums/Direction';

export class LocalFacingPacket extends Packet {
  constructor(protected _facing: Facing) {
    super(PacketType.LocalFacing);
  }

  public serialize() {
    const packet = new ArrayBuffer(3);
    new DataView(packet).setUint8(0, this._type);
    new DataView(packet).setInt8(1, this._facing.x);
    new DataView(packet).setInt8(2, this._facing.y);

    return packet;
  }

  get facing() {
    return this._facing;
  }

  static parse(data: ArrayBuffer) {
    try {
      return new LocalFacingPacket({
        x: new DataView(data).getInt8(0),
        y: new DataView(data).getInt8(1),
      });
    } catch (e) {
      throw new Error(`Malformed packet: ${data}`);
    }
  }
}
