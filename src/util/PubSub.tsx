// Libraries
import React from 'react';
import { v4 as uuid } from 'uuid';

type SubscriptionCallback<TData = any> = (topic: string, data: TData) => void;

interface SubscriptionToken {
  topicKey: string;
  id: string;
}

class Topic<TData = any> {
  private _subscribers = new Map<string, SubscriptionCallback>();

  constructor(private _key: string) {}

  public subscribe(callback: SubscriptionCallback<TData>) {
    const id = uuid();
    this._subscribers.set(id, callback);

    return id;
  }

  public unsubscribe(id: string) {
    return this._subscribers.delete(id);
  }

  public publish(data: TData) {
    for (const [id, cb] of this._subscribers) {
      setTimeout(() => cb(this._key, cb), 0);
    }
  }
}

export class PubSub {
  private _topics = new Map<string, Topic>();

  constructor() {}

  public subscribe<TData>(
    topicKey: string,
    callback: SubscriptionCallback<TData>
  ): SubscriptionToken {
    // Attempt to fetch the topic by key
    let topic = this._topics.get(topicKey);

    // If the topic doesn't exist, create it
    if (!topic) {
      topic = new Topic<TData>(topicKey);
      this._topics.set(topicKey, topic);
    }

    // Subscribe the callback to the topic
    const id = topic.subscribe(callback);

    return {
      topicKey,
      id,
    };
  }

  public unsubscribe(token: SubscriptionToken) {
    const { id, topicKey } = token;

    const topic = this._topics.get(topicKey);
    if (!topic) throw new Error('Invalid token.');

    return topic.unsubscribe(id);
  }

  public publish<TData>(topicKey: string, data: TData) {
    const topic = this._topics.get(topicKey);

    if (!topic) return;

    topic.publish(data);
  }
}

interface PubSubProviderProps {
  children: React.ReactNode;
  pubsub: PubSub;
}

export const PubSubContext = React.createContext(new PubSub());
export const PubSubProvider: React.FC<PubSubProviderProps> = ({
  children,
  pubsub,
}) => {
  return (
    <PubSubContext.Provider value={pubsub}>{children}</PubSubContext.Provider>
  );
};
