// Libraries
import { Actor } from 'excalibur';
import PubSub from 'pubsub-js';
import { LocalCharacter, RemoteCharacter } from '../../actors/Character';
import { Monster } from '../../actors/Monster';

// Types
import type { Game } from '../../entities/Game';
import { Zone } from '../../scenes/Zone';

// Zones (all zones are scenes)
import { PlaygroundScene } from '../../scenes/Playground';

// Scenes (but not all scenes are zones)
import { LoginScene } from '../../scenes/Login';
import { LoadingScene } from '../../scenes/Loading';

export class ZoneController {
  private _tokens: { [subscription: string]: string } = {};

  constructor(protected _game: Game) {
    // Setup listeners that the zone cares about
    this._tokens['scene.change'] = PubSub.subscribe(
      'scene.change',
      this._handleSceneChange.bind(this)
    );
    this._tokens['zone.spawn'] = PubSub.subscribe(
      'zone.spawn',
      this._handleSpawn.bind(this)
    );
    this._tokens['zone.despawn'] = PubSub.subscribe(
      'zone.despawn',
      this._handleDespawn.bind(this)
    );

    // Add all the scenes to the game
    _game.addScene('login', new LoginScene());
    _game.addScene('loading', new LoadingScene());
    _game.addScene('zone_playground', new PlaygroundScene());
  }

  public async cleanup() {
    // Cleanup subscriptions
    Object.values(this._tokens).forEach((token) => PubSub.unsubscribe(token));
  }

  private async _handleSceneChange(_: string, zoneId: string) {
    console.log(`Changing to scene ${zoneId}...`);
    this._game.goToScene(zoneId);
  }

  private async _handleSpawn(_: string, actor: Actor) {
    // If local player
    if (actor instanceof LocalCharacter) {
      console.log('[Local]', actor, 'in', this._game.currentScene);
      this._game.currentScene.add(actor);
    } else if (actor instanceof RemoteCharacter) {
      console.log('[Remote]', actor, 'spawned in', this._game.currentScene);
      this._game.currentScene.add(actor);
    } else if (actor instanceof Monster) {
      console.log('[Mob]', actor, 'spawned in', this._game.currentScene);
      this._game.currentScene.add(actor);
    }
  }

  private async _handleDespawn(_: string, actor: Actor) {
    // If remote player
    if (actor instanceof RemoteCharacter) {
      console.log('[Remote]', actor, 'despawned in', this._game.currentScene);
      this._game.currentScene.remove(actor);
    }
  }
}
