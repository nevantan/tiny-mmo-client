// Libraries
import PubSub from 'pubsub-js';

// Types
import { Mob } from '../../network/graphql/types';

// Objects
import { Monster } from '../../actors/Monster/Monster';
import monsters from '../../actors/Monster';

export class MobController {
  private _tokens: { [subscription: string]: string } = {};

  private _mobs = new Map<string, Monster>();

  constructor() {
    // Listen for mob spawn requests coming from the zone
    this._tokens['network.mob.spawn'] = PubSub.subscribe(
      'network.mob.spawn',
      this._handleMobSpawn.bind(this)
    );
  }

  cleanup() {
    Object.values(this._tokens).forEach((token) => PubSub.unsubscribe(token));
  }

  private _handleMobSpawn(_: string, { id, type, name, position, zone }: Mob) {
    console.log('Mob spawned', id, type, name, position, zone);

    const mob = new monsters[type]({
      id,
      name,
      x: position.x,
      y: position.y,
    });
    this._mobs.set(id, mob);

    // Let the zone controller know we're ready to spawn the mob
    PubSub.publish('zone.spawn', mob);
  }
}
