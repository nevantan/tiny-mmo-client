// Libraries
import PubSub from 'pubsub-js';

// Types
import { LocalCharacter } from '../../actors/Character';
import { NetworkChangeContext } from '../../network';

export class PlayerController {
  // PubSub Listener Tokens
  private _authSuccessToken;
  private _charSelToken;

  // Local data
  private _character?: LocalCharacter;

  constructor() {
    // Listeners

    // Fires when the character is selected from the character selection UI component
    this._charSelToken = PubSub.subscribe(
      'character.selected',
      this._handleCharacterSelected.bind(this)
    );

    // Fires when the TCP socket successfully authenticates and the server has loaded
    // the player's data into memory
    this._authSuccessToken = PubSub.subscribe(
      'network.account.authenticated',
      this._handleAuthSuccess.bind(this)
    );
  }

  public async cleanup() {
    // Cleanup listeners
    PubSub.unsubscribe(this._charSelToken);
    PubSub.unsubscribe(this._authSuccessToken);
  }

  // When the server acknowledges that the socket is authenticated,
  // we're good to load into the zone
  private _handleAuthSuccess(_: string) {
    // This only fires once on initial login, remove the listener
    PubSub.unsubscribe(this._authSuccessToken);

    // If we don't have a character already, something went wrong
    if (!this._character)
      throw new Error('Client-side character selection failed');

    // Let the zone controller know which zone we're loading into
    PubSub.publish('scene.change', this._character.zone);

    // Let the zone controller know we're ready to spawn the local character
    PubSub.publish('zone.spawn', this._character);
  }

  // When the UI signals that we have selected a character
  private _handleCharacterSelected(_: string, charData: any) {
    // Construct the player actor
    this._character = new LocalCharacter({
      ...charData,
      x: charData.position.x,
      y: charData.position.y,
    });
  }
}
