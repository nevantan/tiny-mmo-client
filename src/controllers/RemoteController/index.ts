// Libraries
import PubSub from 'pubsub-js';
import { client } from '../../apollo';

// Node
import { RemoteCharacter } from '../../actors/Character';

// Queries
import {
  RemoteJoinDocument,
  RemoteJoinSubscription,
  RemoteLeaveDocument,
  RemoteLeaveSubscription,
} from '../../network/graphql/types';

export class RemoteController {
  // PubSub Listener Tokens
  private _tokens: { [subscription: string]: string } = {};

  // Local data
  private _remotes = new Map<number, RemoteCharacter>();

  constructor() {
    // Listeners

    // Fires when a remote player joins the zone the local player is currently in
    this._tokens['network.remote.join'] = PubSub.subscribe(
      'network.remote.join',
      this._handleRemoteJoin.bind(this)
    );

    // Fires when a remote player leaves the zone the local player is currently in
    this._tokens['network.remote.leave'] = PubSub.subscribe(
      'network.remote.leave',
      this._handleRemoteLeave.bind(this)
    );

    // Fires when the scene changes
    this._tokens['scene.change'] = PubSub.subscribe(
      'scene.change',
      this._handleSceneChange.bind(this)
    );

    // Setup world listeners after a character has been selected
    this._tokens['character.selected'] = PubSub.subscribe(
      'character.selected',
      this._handleCharacterSelected.bind(this)
    );
  }

  public async cleanup() {
    Object.values(this._tokens).forEach((token) => PubSub.unsubscribe(token));
  }

  // When a remote player joins
  private _handleRemoteJoin(_: string, { uid, charData }: any) {
    console.log('Remote joined', uid, charData);

    const remote = new RemoteCharacter({
      uid,
      ...charData,
      x: charData.position.x,
      y: charData.position.y,
    });
    this._remotes.set(uid, remote);

    // Let the zone controller know we're ready to spawn the remote character
    PubSub.publish('zone.spawn', remote);
  }

  // When a remote player leaves
  private _handleRemoteLeave(_: string, uid: number) {
    // Grab a reference to the remote character
    const remote = this._remotes.get(uid);

    // If the remote player with uid isn't here, don't worry about the rest
    if (!remote) return;

    // Remove the remote from our node list
    this._remotes.delete(uid);

    // Tell the zone controller to remove the remote player
    PubSub.publish('zone.despawn', remote);
  }

  // When the scene changes
  private _handleSceneChange() {
    // Remove all remotes
    this._remotes.clear();
  }

  // When a local character has been selected
  // - Bind world handlers; we don't care about some things until a character has been selected
  private _handleCharacterSelected() {
    // Listen for remotes joining
    client
      .subscribe<RemoteJoinSubscription>({
        query: RemoteJoinDocument,
      })
      .subscribe(({ data }) => {
        if (!data) return;

        const { uid, ...charData } = data.remoteJoined;
        this._handleRemoteJoin('', {
          uid,
          charData,
        });
      });

    // Listen for remotes leaving
    client
      .subscribe<RemoteLeaveSubscription>({
        query: RemoteLeaveDocument,
      })
      .subscribe(({ data }) => {
        if (!data) return;

        this._handleRemoteLeave('', data.remoteLeft);
      });
  }
}
