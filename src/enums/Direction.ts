export enum Direction {
  Left = -1,
  Right = 1,
  Up = -1,
  Down = 1,
}

export const FacingString = {
  x: {
    [-1]: 'left',
    [1]: 'right',
  },
  y: {
    [-1]: 'up',
    [1]: 'down',
  },
};

export interface Facing {
  x: number;
  y: number;
}
