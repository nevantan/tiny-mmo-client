// Libraries
import React from 'react';
import ReactDOM from 'react-dom/client';
import { ApolloProvider } from '@apollo/client';
import { AuthPacket, SelectCharacterPacket } from './network/Packet';
import PubSub from 'pubsub-js';

// UI Imports
import { UI } from './ui';
import { RouteProvider } from './ui/context/RouteContext';
import { NetworkProvider } from './network/Provider';

// Game Imports
import { Game } from './entities/Game';

// Networking
import { NetworkModule } from './network';
import { client } from './apollo';
const network = new NetworkModule();

// Initialize the rendering/game engine
const game = new Game();

// Initialize the UI
ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <ApolloProvider client={client}>
      <NetworkProvider network={network}>
        <RouteProvider>
          <UI />
        </RouteProvider>
      </NetworkProvider>
    </ApolloProvider>
  </React.StrictMode>
);
