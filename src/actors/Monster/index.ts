import { Monster } from './Monster';

import { GreenSlime } from './GreenSlime';

export { Monster, GreenSlime };

export default {
  Monster,
  GreenSlime,
};
