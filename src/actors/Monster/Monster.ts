// Libraries
import { Actor, ActorArgs, Animation, Vector } from 'excalibur';

// Types
import type { Game } from '../../entities/Game';
import { Facing, Direction } from '../../enums/Direction';

interface MobArgs extends ActorArgs {
  id: string;
  name: string;
}

export class Monster extends Actor {
  protected _id;
  protected _displayName;

  protected _velocity = new Vector(0, 0);
  protected _facing: Facing = {
    x: Direction.Right,
    y: Direction.Down,
  };
  protected _animations?: { [animName: string]: Animation };

  constructor({ id, name, ...config }: MobArgs) {
    super(config);

    this._id = id;
    this._displayName = name;

    this.z = 10;
  }

  public onInitialize(game: Game) {
    // At this point, animations would have been built
    // If they aren't, something is wrong
    if (!this._animations)
      throw new Error('Animations should have been built at this point.');

    // Scale up each animation and add it to the graphics
    for (const [key, anim] of Object.entries(this._animations)) {
      anim.scale = new Vector(4, 4);
      this.graphics.add(key, anim);
    }

    // Show the default animation
    this.graphics.use('idle_down_right');
  }
}
