// Libraries
import { Game } from '../../../entities/Game';

// Parent
import { Monster } from '../Monster';

// Utils
import { buildAnims } from './resources';

export class GreenSlime extends Monster {
  public onInitialize(game: Game) {
    this._animations = buildAnims();

    super.onInitialize(game);
  }
}
