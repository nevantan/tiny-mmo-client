// Libraries
import {
  Animation,
  AnimationStrategy,
  ImageSource,
  Loader,
  SpriteSheet,
} from 'excalibur';

// Assets
import GreenSlimeAttack from '../../../assets/monsters/green_slime/attack.png';
import GreenSlimeDie from '../../../assets/monsters/green_slime/die.png';
import GreenSlimeHurt from '../../../assets/monsters/green_slime/hurt.png';
import GreenSlimeIdle from '../../../assets/monsters/green_slime/idle.png';

export const Resources = {
  greenSlimeAttack: new ImageSource(GreenSlimeAttack),
  greenSlimeDie: new ImageSource(GreenSlimeDie),
  greenSlimeHurt: new ImageSource(GreenSlimeHurt),
  greenSlimeIdle: new ImageSource(GreenSlimeIdle),
};

export const GreenSlimeAttackSS = SpriteSheet.fromImageSource({
  image: Resources.greenSlimeAttack,
  grid: {
    columns: 4,
    rows: 4,
    spriteWidth: 32,
    spriteHeight: 32,
  },
});

export const GreenSlimeDieSS = SpriteSheet.fromImageSource({
  image: Resources.greenSlimeDie,
  grid: {
    columns: 9,
    rows: 1,
    spriteWidth: 32,
    spriteHeight: 32,
  },
});

export const GreenSlimeHurtSS = SpriteSheet.fromImageSource({
  image: Resources.greenSlimeHurt,
  grid: {
    columns: 4,
    rows: 4,
    spriteWidth: 32,
    spriteHeight: 32,
  },
});

export const GreenSlimeIdleSS = SpriteSheet.fromImageSource({
  image: Resources.greenSlimeIdle,
  grid: {
    columns: 8,
    rows: 4,
    spriteWidth: 32,
    spriteHeight: 32,
  },
});

export const loader = new Loader();
for (const res of Object.values(Resources)) {
  loader.addResource(res);
}

export const buildAnims = () => ({
  attack_down_right: Animation.fromSpriteSheet(
    GreenSlimeAttackSS,
    [0, 1, 2, 3],
    1000 / 10,
    AnimationStrategy.End
  ),
  attack_down_left: Animation.fromSpriteSheet(
    GreenSlimeAttackSS,
    [4, 5, 6, 7],
    1000 / 10,
    AnimationStrategy.End
  ),
  attack_up_right: Animation.fromSpriteSheet(
    GreenSlimeAttackSS,
    [8, 9, 10, 11],
    1000 / 10,
    AnimationStrategy.End
  ),
  attack_up_left: Animation.fromSpriteSheet(
    GreenSlimeAttackSS,
    [12, 13, 14, 15],
    1000 / 10,
    AnimationStrategy.End
  ),
  die: Animation.fromSpriteSheet(
    GreenSlimeDieSS,
    [0, 1, 2, 3, 4, 5, 6, 7, 8],
    1000 / 10,
    AnimationStrategy.End
  ),
  idle_down_right: Animation.fromSpriteSheet(
    GreenSlimeIdleSS,
    Array.from({ length: 8 }, (_, i) => i),
    1000 / 4,
    AnimationStrategy.Loop
  ),
  idle_down_left: Animation.fromSpriteSheet(
    GreenSlimeIdleSS,
    Array.from({ length: 8 }, (_, i) => i + 8),
    1000 / 4,
    AnimationStrategy.Loop
  ),
  idle_up_right: Animation.fromSpriteSheet(
    GreenSlimeIdleSS,
    Array.from({ length: 8 }, (_, i) => i + 16),
    1000 / 4,
    AnimationStrategy.Loop
  ),
  idle_up_left: Animation.fromSpriteSheet(
    GreenSlimeIdleSS,
    Array.from({ length: 8 }, (_, i) => i + 24),
    1000 / 4,
    AnimationStrategy.Loop
  ),
  walk_down_right: Animation.fromSpriteSheet(
    GreenSlimeIdleSS,
    [0, 1, 2, 3],
    1000 / 5,
    AnimationStrategy.Loop
  ),
  walk_down_left: Animation.fromSpriteSheet(
    GreenSlimeIdleSS,
    [8, 9, 10, 11],
    1000 / 5,
    AnimationStrategy.Loop
  ),
  walk_up_right: Animation.fromSpriteSheet(
    GreenSlimeIdleSS,
    [16, 17, 18, 19],
    1000 / 5,
    AnimationStrategy.Loop
  ),
  walk_up_left: Animation.fromSpriteSheet(
    GreenSlimeIdleSS,
    [24, 25, 26, 27],
    1000 / 5,
    AnimationStrategy.Loop
  ),
});
