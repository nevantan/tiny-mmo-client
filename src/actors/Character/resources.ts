// Libraries
import {
  Animation,
  AnimationStrategy,
  ImageSource,
  Loader,
  SpriteSheet,
} from 'excalibur';

// Assets
import BeginnerAttack from '../../assets/character/beginner/attack.png';
import BeginnerDie from '../../assets/character/beginner/die.png';
import BeginnerHurt from '../../assets/character/beginner/hurt.png';
import BeginnerIdle from '../../assets/character/beginner/idle.png';
import BeginnerWalk from '../../assets/character/beginner/walk.png';

export const Resources = {
  beginnerAttack: new ImageSource(BeginnerAttack),
  beginnerDie: new ImageSource(BeginnerDie),
  beginnerHurt: new ImageSource(BeginnerHurt),
  beginnerIdle: new ImageSource(BeginnerIdle),
  beginnerWalk: new ImageSource(BeginnerWalk),
};

export const BeginnerAttackSS = SpriteSheet.fromImageSource({
  image: Resources.beginnerAttack,
  grid: {
    columns: 4,
    rows: 4,
    spriteWidth: 32,
    spriteHeight: 32,
  },
});

export const BeginnerDieSS = SpriteSheet.fromImageSource({
  image: Resources.beginnerDie,
  grid: {
    columns: 12,
    rows: 1,
    spriteWidth: 32,
    spriteHeight: 32,
  },
});

export const BeginnerHurtSS = SpriteSheet.fromImageSource({
  image: Resources.beginnerHurt,
  grid: {
    columns: 4,
    rows: 4,
    spriteWidth: 32,
    spriteHeight: 32,
  },
});

export const BeginnerIdleSS = SpriteSheet.fromImageSource({
  image: Resources.beginnerIdle,
  grid: {
    columns: 16,
    rows: 4,
    spriteWidth: 32,
    spriteHeight: 32,
  },
});

export const BeginnerWalkSS = SpriteSheet.fromImageSource({
  image: Resources.beginnerWalk,
  grid: {
    columns: 4,
    rows: 4,
    spriteWidth: 32,
    spriteHeight: 32,
  },
});

export const loader = new Loader();
for (const res of Object.values(Resources)) {
  loader.addResource(res);
}

export const buildAnims = () => ({
  attack_down_right: Animation.fromSpriteSheet(
    BeginnerAttackSS,
    [0, 1, 2, 3],
    1000 / 10,
    AnimationStrategy.End
  ),
  attack_down_left: Animation.fromSpriteSheet(
    BeginnerAttackSS,
    [4, 5, 6, 7],
    1000 / 10,
    AnimationStrategy.End
  ),
  attack_up_right: Animation.fromSpriteSheet(
    BeginnerAttackSS,
    [8, 9, 10, 11],
    1000 / 10,
    AnimationStrategy.End
  ),
  attack_up_left: Animation.fromSpriteSheet(
    BeginnerAttackSS,
    [12, 13, 14, 15],
    1000 / 10,
    AnimationStrategy.End
  ),
  die: Animation.fromSpriteSheet(
    BeginnerDieSS,
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
    1000 / 10,
    AnimationStrategy.End
  ),
  idle_down_right: Animation.fromSpriteSheet(
    BeginnerIdleSS,
    Array.from({ length: 16 }, (_, i) => i),
    1000 / 4,
    AnimationStrategy.Loop
  ),
  idle_down_left: Animation.fromSpriteSheet(
    BeginnerIdleSS,
    Array.from({ length: 16 }, (_, i) => i + 16),
    1000 / 4,
    AnimationStrategy.Loop
  ),
  idle_up_right: Animation.fromSpriteSheet(
    BeginnerIdleSS,
    Array.from({ length: 16 }, (_, i) => i + 32),
    1000 / 4,
    AnimationStrategy.Loop
  ),
  idle_up_left: Animation.fromSpriteSheet(
    BeginnerIdleSS,
    Array.from({ length: 16 }, (_, i) => i + 48),
    1000 / 4,
    AnimationStrategy.Loop
  ),
  walk_down_right: Animation.fromSpriteSheet(
    BeginnerWalkSS,
    [0, 1, 2, 3],
    1000 / 5,
    AnimationStrategy.Loop
  ),
  walk_down_left: Animation.fromSpriteSheet(
    BeginnerWalkSS,
    [4, 5, 6, 7],
    1000 / 5,
    AnimationStrategy.Loop
  ),
  walk_up_right: Animation.fromSpriteSheet(
    BeginnerWalkSS,
    [8, 9, 10, 11],
    1000 / 5,
    AnimationStrategy.Loop
  ),
  walk_up_left: Animation.fromSpriteSheet(
    BeginnerWalkSS,
    [12, 13, 14, 15],
    1000 / 5,
    AnimationStrategy.Loop
  ),
});
