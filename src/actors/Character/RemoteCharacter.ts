// Libraries
import { Actor, ActorArgs, Vector } from 'excalibur';
import { Character } from './Character';
import PubSub from 'pubsub-js';

// Types
import type { Game } from '../../entities/Game';
import { Facing, Direction } from '../../enums/Direction';

// Assets
import { buildAnims } from './resources';

export interface RemoteCharacterArgs extends ActorArgs {
  uid: number;
}

export class RemoteCharacter extends Character {
  private _uid: number;
  private _speed = 200;
  private _target = new Vector(this.pos.x, this.pos.y);

  constructor(config: RemoteCharacterArgs) {
    const { uid, ...args } = config;

    super(args);

    this._uid = uid;

    this.z = 14;
  }

  public onInitialize(game: Game) {
    super.onInitialize(game);

    console.log(`Remote ${this._uid} listening for updates`);
    PubSub.subscribe(
      `network.remote.${this._uid}`,
      this._handleServerCommand.bind(this)
    );
  }

  public onPreUpdate(game: Game, delta: number) {
    const deltaS = delta / 1000;

    // If there is velocity, update the character position
    if (this._velocity.size > 0) {
      this.pos.x += this._velocity.normalize().x * this._speed * deltaS;
      this.pos.y += this._velocity.normalize().y * this._speed * deltaS;
    } else {
      // If we're close to the correction target, jump there
      if (Vector.distance(this.pos, this._target) < 10) {
        this.pos = new Vector(this._target.x, this._target.y);
      } else {
        // Else, start easing toward correction target
        const velocity = new Vector(
          this._target.x - this.pos.x,
          this._target.y - this.pos.y
        ).normalize();
        this.pos.x += velocity.x * this._speed * deltaS;
        this.pos.y += velocity.y * this._speed * deltaS;
      }
    }

    // Set the animation with direction
    if (this._velocity.size > 0)
      this.graphics.use(
        `walk_${this._facing.y < 0 ? 'up' : 'down'}_${
          this._facing.x < 0 ? 'left' : 'right'
        }`
      );
    else
      this.graphics.use(
        `idle_${this._facing.y < 0 ? 'up' : 'down'}_${
          this._facing.x < 0 ? 'left' : 'right'
        }`
      );
  }

  private _handleServerCommand(topic: string, data: any) {
    switch (topic) {
      case `network.remote.${this._uid}.facing`:
        this._facing = data;
        break;
      case `network.remote.${this._uid}.velocity`:
        this._velocity = data.velocity;
        this._target = data.position;
        break;
      default:
        console.error('Unsupported server command:', topic, data);
    }
  }
}
