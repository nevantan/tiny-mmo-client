// Libraries
import { ActorArgs, Input, Shape, Vector } from 'excalibur';
import { Character } from './Character';
import PubSub from 'pubsub-js';

// Types
import type { Game } from '../../entities/Game';
import { Direction, Facing, FacingString } from '../../enums/Direction';
import { Tilemap } from '../../entities/Tilemap';
import { Zone } from '../../scenes/Zone';

// Packets
import { LocalFacingPacket, LocalVelocityPacket } from '../../network/Packet';

export interface LocalCharacterArgs extends ActorArgs {
  zone: string;
}

export class LocalCharacter extends Character {
  private _zone;
  private _lastVelocity = new Vector(0, 0);
  private _speed = 200;
  private _lastFacing: Facing = { ...this._facing };
  private _pointer = new Vector(0, 0);
  private _serverPos?: Vector;

  constructor(config: LocalCharacterArgs) {
    const { zone, ...actorArgs } = config;
    const collider = Shape.Box(24, 24);

    super({
      ...actorArgs,
      collider,
    });

    this._zone = zone;

    this.z = 15;
  }

  public onInitialize(game: Game) {
    super.onInitialize(game);

    // Setup pointer event listeners
    game.input.pointers.primary.on('move', (e) => {
      this._pointer = e.coordinates.worldPos;
    });

    // Setup server correction listener
    PubSub.subscribe(
      'network.local.correction',
      this._handleServerCorrection.bind(this)
    );
  }

  public onPreUpdate(game: Game, delta: number) {
    let map;
    if (game.currentScene instanceof Zone) {
      map = (game.currentScene as Zone).map;
    }

    const deltaS = delta / 1000;

    // Determine velocity from player input
    this._velocity = new Vector(0, 0);
    if (game.input.keyboard.isHeld(Input.Keys.A)) this._velocity.x -= 1;
    if (game.input.keyboard.isHeld(Input.Keys.D)) this._velocity.x += 1;
    if (game.input.keyboard.isHeld(Input.Keys.W)) this._velocity.y -= 1;
    if (game.input.keyboard.isHeld(Input.Keys.S)) this._velocity.y += 1;

    // If there has been a velocity change since last reported to server
    if (
      this._velocity.x !== this._lastVelocity.x ||
      this._velocity.y !== this._lastVelocity.y
    ) {
      // Send new velocity to server
      PubSub.publish('network.send', new LocalVelocityPacket(this._velocity));

      // Save the last value we sent to the server
      this._lastVelocity = new Vector(this._velocity.x, this._velocity.y);
    }

    // Normalize the velocity vector
    if (this._velocity.size > 0) this._velocity = this._velocity.normalize();

    // Update character position if there is movement
    if (this._velocity.size > 0) {
      this._moveWithCollision(deltaS, map);

      // Even though we're still moving, let's do a quick check to see if we're way off
      // (4 tiles)
      if (this._serverPos && Vector.distance(this._serverPos, this.pos) > 64) {
        // We've drifted off from the server, rubber band to correct
        this.pos = this._serverPos.clone();
      } else {
        // The different is small enough that it doesn't matter, clear the server correction
        this._serverPos = undefined;
      }
    } else if (this._serverPos) {
      // If we aren't moving client-side, start correcting toward server position
      const correction = new Vector(
        this._serverPos.x - this.pos.x,
        this._serverPos.y - this.pos.y
      );
      if (correction.size > 0) {
        if (correction.size < this._speed * deltaS) {
          this.pos = this._serverPos.clone();
          this._serverPos = undefined;
        } else {
          this.pos.x += correction.normalize().x * this._speed * deltaS;
          this.pos.y += correction.normalize().y * this._speed * deltaS;
        }
      }
    }

    // Determine facing direction based on mouse
    this._facing.x =
      this.pos.x - this._pointer.x > 0 ? Direction.Left : Direction.Right;
    this._facing.y =
      this.pos.y - this._pointer.y > 0 ? Direction.Up : Direction.Down;

    // If the facing direction changed since reported to server
    if (
      this._facing.x !== this._lastFacing.x ||
      this._facing.y !== this._lastFacing.y
    ) {
      // Send new facing
      PubSub.publish('network.send', new LocalFacingPacket(this._facing));

      // Save the last value we sent to the server
      this._lastFacing = {
        x: this._facing.x,
        y: this._facing.y,
      };
    }

    // Set the animation with direction
    if (this._velocity.size > 0)
      this.graphics.use(
        `walk_${this._facing.y < 0 ? 'up' : 'down'}_${
          this._facing.x < 0 ? 'left' : 'right'
        }`
      );
    else
      this.graphics.use(
        `idle_${this._facing.y < 0 ? 'up' : 'down'}_${
          this._facing.x < 0 ? 'left' : 'right'
        }`
      );
  }

  private _moveWithCollision(delta: number, map?: Tilemap) {
    // If we were provided a collision map, move with collision
    if (map) {
      const speed = this._speed * delta;
      const stepCount = 3;
      const stepLength = speed / stepCount;

      // Simulate multiple steps in this update
      for (let i = 0; i < stepCount; i++) {
        // Save our previous position
        const prevPos = this.pos.clone();

        // Simulate X movement, update collider, and test collision
        this.pos.x += this._velocity.x * stepLength;
        this.collider.update();
        if (this._testCollision(map)) {
          this.pos.x = prevPos.x;
          this.collider.update();
        }

        // Simulate Y movement and test collision
        this.pos.y += this._velocity.y * stepLength;
        this.collider.update();
        if (this._testCollision(map)) {
          this.pos.y = prevPos.y;
          this.collider.update();
        }
      }
    } else {
      // Else, just update position
      this.pos.x += this._velocity.x * this._speed * delta;
      this.pos.y += this._velocity.y * this._speed * delta;
    }
  }

  private _testCollision(map: Tilemap) {
    const { top, right, bottom, left } = this.collider.bounds;
    const scaledTile = map.tileSize * 4;

    const leftTile = Math.floor(Math.max(0, left / scaledTile));
    const rightTile = Math.floor(Math.min(right / scaledTile, map.width));
    const topTile = Math.floor(Math.max(0, top / scaledTile));
    const bottomTile = Math.floor(Math.min(bottom / scaledTile, map.height));

    for (let x = leftTile; x <= rightTile; x++) {
      for (let y = topTile; y <= bottomTile; y++) {
        if (map.isSolidAt(x, y)) return true;
      }
    }

    return false;
  }

  private _handleServerCorrection(topic: string, data: any) {
    switch (topic) {
      case 'network.local.correction.facing':
        this._facing = data as Facing;
        break;
      case 'network.local.correction.position':
        this._serverPos = new Vector(data.x, data.y);
        break;
      default:
        console.error('Unsupported server correction:', data);
    }
  }

  public get zone() {
    return this._zone;
  }
}
