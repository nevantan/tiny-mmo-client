// Libraries
import { Actor, ActorArgs, Vector } from 'excalibur';

// Types
import type { Game } from '../../entities/Game';
import { Facing, Direction } from '../../enums/Direction';

// Assets
import { buildAnims } from './resources';

export class Character extends Actor {
  protected _velocity = new Vector(0, 0);
  protected _facing: Facing = {
    x: Direction.Right,
    y: Direction.Down,
  };

  constructor(config?: ActorArgs) {
    super(config);
  }

  public onInitialize(game: Game) {
    const animations = buildAnims();

    // Scale up each animation and add it to the graphics
    for (const [key, anim] of Object.entries(animations)) {
      anim.scale = new Vector(4, 4);
      this.graphics.add(key, anim);
    }

    // Show the default animation
    this.graphics.use('idle_down_right');
  }
}
