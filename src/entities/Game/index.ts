// Libraries
import { Engine, EngineOptions, Loader, ImageSource, Input } from 'excalibur';
import { DevTool } from '@excaliburjs/dev-tools';
import PubSub from 'pubsub-js';

// Controllers
import { MobController } from '../../controllers/MobController';
import { PlayerController } from '../../controllers/PlayerController';
import { RemoteController } from '../../controllers/RemoteController';
import { ZoneController } from '../../controllers/ZoneController';

// Types
import { Route } from '../../ui/context/RouteContext';
import { NetworkStatus, NetworkChangeContext } from '../../network';

// Initial Assets
import { loader as characterLoader } from '../../actors/Character';
import { loader as slimeLoader } from '../../actors/Monster/GreenSlime/resources';

interface GameOptions extends EngineOptions {}

export class Game extends Engine {
  private _devtool;
  private _initialLoader: Loader;

  private _mobController = new MobController();
  private _playerController = new PlayerController();
  private _remoteController = new RemoteController();
  private _zoneController = new ZoneController(this);

  constructor(opts?: GameOptions) {
    const { ...engineOpts } = opts ?? {};
    super({
      resolution: {
        width: 1920,
        height: 1080,
      },
      canvasElementId: 'game',
      antialiasing: false,
      pointerScope: Input.PointerScope.Document,
      ...engineOpts,
    });

    // Loader
    this._initialLoader = new Loader([characterLoader, slimeLoader]);

    // Pubsub listeners
    PubSub.subscribe('network.status', this._handleNetworkChange.bind(this));

    this.start(this._initialLoader).then(() => {
      // Set the initial game scene and UI route
      PubSub.publish('route.change', { route: Route.Login });
      this.goToScene('login');
    });

    this._devtool = new DevTool(this);
  }

  private async _handleNetworkChange(
    _: string,
    { status, code, reason }: NetworkChangeContext
  ) {
    // If the network change was a disconnect
    if (status === NetworkStatus.Disconnected) {
      // Clean up controllers
      await this._playerController.cleanup();
      await this._remoteController.cleanup();
      await this._zoneController.cleanup();

      // Recreate controllers
      this._playerController = new PlayerController();
      this._remoteController = new RemoteController();
      this._zoneController = new ZoneController(this);

      // Change to login scene
      this.goToScene('login');
    }
  }
}
