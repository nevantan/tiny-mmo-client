import { Entity, TransformComponent, GraphicsComponent } from 'excalibur';

export class Tile extends Entity {
  private _graphics = new GraphicsComponent();
  private _transform = new TransformComponent();

  constructor() {
    super();

    this.addComponent(this._transform);
    this.addComponent(this._graphics);
  }

  onInitialize() {}

  get graphics() {
    return this._graphics;
  }

  set x(n: number) {
    this._transform.pos.x = n;
  }

  set y(n: number) {
    this._transform.pos.y = n;
  }
}
