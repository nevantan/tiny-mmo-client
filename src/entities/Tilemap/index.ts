// Libraries
import {
  Entity,
  SpriteSheet,
  GraphicsComponent,
  TransformComponent,
  Vector,
} from 'excalibur';

// Child components
import { Tile } from '../Tile';

// Types
import { Game } from '../Game';
import { MapData } from '../../scenes/Zone/types';

interface TilemapConfig {
  data: MapData;
  tileset: SpriteSheet;
  scale?: Vector;
}

export class Tilemap extends Entity {
  private _data;
  private _tileset;
  private _tilesetWidth;
  private _collision = new Map<string, boolean>();

  private _transform = new TransformComponent();
  private _graphics = new GraphicsComponent({
    onPostDraw: (ctx, delta) => this.draw(ctx, delta),
  });

  constructor({ data, tileset, scale }: TilemapConfig) {
    super();

    this._data = data;
    this._tileset = tileset;
    this._tilesetWidth = tileset.columns;

    this.addComponent(this._transform);
    this.addComponent(this._graphics);

    if (scale) this._transform.scale = new Vector(scale.x, scale.y);
  }

  public onInitialize(game: Game) {
    // Calculate a flattened collision map from the map data:
    // For each solid layer, determine whether there is a tile at the given coords
    for (const layer of this._data.layers.filter((layer) => layer.solid)) {
      // Loop through the tiles in the solid layer and load them into the collision map
      for (const { x, y } of layer.positions) {
        this._collision.set(`${x}:${y}`, true);
      }
    }
  }

  // Determine whether a given coordinate is solid
  public isSolidAt(x: number, y: number) {
    // If the key derived from x and y exists in the collision map, it is solid
    return this._collision.has(`${x}:${y}`);
  }

  public draw(ctx: ex.ExcaliburGraphicsContext, delta: number) {
    ctx.save();
    ctx.z = 0;

    // For each layer
    for (const layer of this._data.layers) {
      // For each tile in the layer
      for (const { id, x, y } of layer.positions) {
        const graphic = this._tileset.getSprite(
          id % this._tilesetWidth,
          Math.floor(id / this._tilesetWidth)
        );
        if (graphic) {
          graphic.scale = this.scale;
          graphic.draw(
            ctx,
            x * this._data.tile_size * this.scale.x,
            y * this._data.tile_size * this.scale.y
          );
        }
      }
    }

    ctx.restore();
  }

  get scale() {
    return this._transform.scale;
  }

  get width() {
    return this._data.map_width;
  }

  get height() {
    return this._data.map_height;
  }

  get tileSize() {
    return this._data.tile_size;
  }
}
