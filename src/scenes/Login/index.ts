// Libraries
import { Font, Label, Scene, Vector } from 'excalibur';

// Types
import type { Game } from '../../entities/Game';

export class LoginScene extends Scene {
  public onInitialize(game: Game) {
    this.add(
      new Label({
        text: 'LoginScene',
        pos: new Vector(game.halfCanvasWidth, game.halfCanvasHeight),
        font: new Font({
          family: 'Lora',
          size: 24,
        }),
      })
    );
  }
}
