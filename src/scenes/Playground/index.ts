// Libraries
import { ImageSource, SpriteSheet, Loader } from 'excalibur';
import { Zone } from '../Zone';

// Types
import type { Game } from '../../entities/Game';

// Resources
import tileset from '../../assets/zones/zone_playground/tileset.png';
import mapData from '../../assets/zones/zone_playground/map.json';

import { GreenSlime } from '../../actors/Monster/GreenSlime';

export class PlaygroundScene extends Zone {
  constructor() {
    super('zone_playground');
  }

  public onInitialize(game: Game) {
    const image = new ImageSource(tileset);
    this._mapData = mapData;

    game.start(new Loader([image])).then(() => {
      this._tileset = SpriteSheet.fromImageSource({
        image,
        grid: {
          columns: 8,
          rows: 4,
          spriteWidth: 8,
          spriteHeight: 8,
        },
      });

      super.onInitialize(game);
    });
  }

  public async onActivate() {
    // Call the parent activation to setup networking
    super.onActivate();

    // We want to load this scene in
    // Currently, we only have the local player
  }
}
