// Libraries
import { Font, Label, Scene, SpriteSheet, Vector } from 'excalibur';
import { Tilemap } from '../../entities/Tilemap';
import PubSub from 'pubsub-js';

// Types
import type { Game } from '../../entities/Game';
import { MapData } from './types';

// Networking
import { client } from '../../apollo';

// Queries
import {
  GetZoneDataDocument,
  GetZoneDataQuery,
} from '../../network/graphql/types';

export class Zone extends Scene {
  protected _tileset: SpriteSheet | null = null;
  protected _mapData: MapData = {
    id: '',
    map_width: 0,
    map_height: 0,
    tile_size: 0,
    layers: [],
  };
  protected _map?: Tilemap;

  constructor(private _id: string) {
    super();
  }

  public onInitialize(game: Game) {
    // The child class will have already loaded the map and tileset at this point - render it
    this._map = new Tilemap({
      data: this._mapData,
      tileset: this._tileset!,
      scale: new Vector(2, 2),
    });
    this.add(this._map);

    this.add(
      new Label({
        text: this._id,
        pos: new Vector(game.halfCanvasWidth, game.halfCanvasHeight),
        font: new Font({
          family: 'Lora',
          size: 24,
        }),
      })
    );
  }

  async onActivate() {
    // Make query to fetch current data from server
    const { data } = await client.query<GetZoneDataQuery>({
      query: GetZoneDataDocument,
      variables: {
        id: this._id,
      },
    });

    if (!data || !data.zone) {
      console.log('Failed to load zone:', this._id, data);
      return;
    }

    console.log('Zone loaded:', data);

    // Spawn in remotes that are already in the zone when the player joins
    for (const character of data.zone.characters) {
      if (!character) continue;
      const { uid, ...charData } = character;

      PubSub.publish('network.remote.join', { uid, charData });
    }

    // Spawn in mobs that are already in the zone when the player joins
    for (const mob of data.zone.mobs) {
      if (!mob) continue;

      PubSub.publish('network.mob.spawn', mob);
    }

    // Subscribe to messages about what's happening in the zone
    PubSub.subscribe('network.zone', this._handleNetworkMessage.bind(this));
  }

  private _handleNetworkMessage(topic: string, _: any) {
    switch (topic) {
      case 'network.zone.remoteJoin':
    }
  }

  get map() {
    return this._map;
  }
}
