// Types
export interface TileData {
  x: number;
  y: number;
  id: number;
  type?: string;
}

export interface MapLayer {
  name: string;
  solid: boolean;
  positions: TileData[];
}

export interface MapData {
  id: string;
  tile_size: number;
  map_width: number;
  map_height: number;
  layers: MapLayer[];
}
