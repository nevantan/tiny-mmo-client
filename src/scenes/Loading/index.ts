// Libraries
import { Font, Label, Scene, Vector } from 'excalibur';

// Types
import type { Game } from '../../entities/Game';

export class LoadingScene extends Scene {
  public onInitialize(game: Game) {
    this.add(
      new Label({
        text: 'LoadingScene',
        pos: new Vector(game.halfCanvasWidth, game.halfCanvasHeight),
        font: new Font({
          family: 'Lora',
          size: 24,
        }),
      })
    );
  }
}
