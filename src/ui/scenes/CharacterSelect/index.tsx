// Libraries
import { useContext } from 'react';
import PubSub from 'pubsub-js';

// Context
import { Route, RouteContext } from '../../context/RouteContext';

// Queries
import {
  useGetCharactersQuery,
  useSelectCharacterMutation,
} from '../../../network/graphql/types';

export const CharacterSelectScene = () => {
  const { setRoute } = useContext(RouteContext);
  const { loading, data } = useGetCharactersQuery();
  const [selectCharacter, { loading: selectingCharacter }] =
    useSelectCharacterMutation();

  const handleCharacterSelection = async (uid: number) => {
    // GraphQL query to select the character
    const { data } = await selectCharacter({
      variables: { uid },
    });

    if (!data) throw new Error('Failed to select character!');

    // Let the PlayerController know that a character has been selected
    PubSub.publish('character.selected', data.selectCharacter);

    // Establish the TCP socket to the server
    PubSub.publish('network.connect');

    // Show the loading screen while we wait for the socket to be established
    setRoute(Route.Loading);
    PubSub.publish('scene.change', 'loading');
  };

  return (
    <div className="panel">
      {loading ? (
        <div>Loading characters...</div>
      ) : selectingCharacter ? (
        <div>Entering world...</div>
      ) : (
        <ul className="characters">
          {data?.characters.map((character) => (
            <li>
              <button onClick={() => handleCharacterSelection(character!.uid)}>
                {character!.taggedName}
              </button>
            </li>
          ))}
        </ul>
      )}
    </div>
  );
};

export default CharacterSelectScene;
