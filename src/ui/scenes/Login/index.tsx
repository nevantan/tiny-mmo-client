// Libraries
import React, { useContext, useState } from 'react';
import { Route, RouteContext } from '../../context/RouteContext';

// Types
import { NetworkStatus } from '../../../network';

// Queries
import { useLoginMutation } from '../../../network/graphql/types';

export const LoginScene = () => {
  const { setRoute, ctx } = useContext(RouteContext);

  const [login, { loading }] = useLoginMutation();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async (e: React.FormEvent) => {
    // Stop the page from reloading when the login form is submitted
    e.preventDefault();

    try {
      const { data, errors } = await login({
        variables: {
          email,
          password,
        },
      });

      if (errors || !data)
        throw new Error('Invalid username/password combination');

      sessionStorage.setItem('access_token', data.login);

      setRoute(Route.CharacterSelect);
    } catch (e: any) {
      console.log(e);
      console.error(e.message);
    }
  };

  const handleTestLogin = (id: number) => {
    switch (id) {
      case 1:
        setEmail(import.meta.env.VITE_TEST_EMAIL);
        setPassword(import.meta.env.VITE_TEST_PASSWORD);
        // handleSubmit({
        //   preventDefault: () => null,
        // } as unknown as React.FormEvent);
        break;
      case 2:
        setEmail(import.meta.env.VITE_TEST_EMAIL2);
        setPassword(import.meta.env.VITE_TEST_PASSWORD);
        // handleSubmit({
        //   preventDefault: () => null,
        // } as unknown as React.FormEvent);
        break;
    }
  };

  return (
    <div className="page">
      {ctx.status === NetworkStatus.Disconnected && (
        <div className="error">
          You have been disconnected from the server: [{ctx.code}] {ctx.reason}
        </div>
      )}

      <form onSubmit={handleSubmit}>
        <div className="group">
          <label htmlFor="email">Email</label>
          <input
            type="email"
            id="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </div>

        <div className="group">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            id="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>

        <button>{loading ? 'Logging in...' : 'Login'}</button>

        <button onClick={() => handleTestLogin(1)}>
          {import.meta.env.VITE_TEST_EMAIL}
        </button>
        <button onClick={() => handleTestLogin(2)}>
          {import.meta.env.VITE_TEST_EMAIL2}
        </button>
      </form>
    </div>
  );
};

export default LoginScene;
