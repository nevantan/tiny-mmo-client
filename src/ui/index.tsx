// Libraries
import { useContext, useState } from 'react';
import { Route, RouteContext } from './context/RouteContext';

// Scenes
import CharacterSelectScene from './scenes/CharacterSelect';
import LoginScene from './scenes/Login';

// Styles
import './index.scss';

export const UI = () => {
  const { route } = useContext(RouteContext);

  if (route === Route.Login) return <LoginScene />;
  else if (route === Route.CharacterSelect) return <CharacterSelectScene />;

  return <div />;
};
