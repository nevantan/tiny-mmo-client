// Libraries
import React, { useEffect, useState } from 'react';
import PubSub from 'pubsub-js';

// Types
import { NetworkStatus, NetworkChangeContext } from '../../network';

export enum Route {
  None = '',

  CharacterSelect = 'character_select',
  Game = 'game',
  Loading = 'loading',
  Login = 'login',
  Start = 'start',
}

export const RouteContext = React.createContext<{
  route: Route;
  setRoute: any;
  ctx: { [key: string]: any };
}>({
  route: Route.None,
  setRoute: () => null,
  ctx: {},
});

interface RouteProviderProps {
  children: React.ReactNode;
  initial?: Route;
}

export const RouteProvider: React.FC<RouteProviderProps> = ({
  children,
  initial,
}) => {
  const [route, setRoute] = useState<Route>(initial ?? Route.Start);
  const [ctx, setContext] = useState<{ [key: string]: any }>({});

  const handleNetworkStatus = (
    _: string,
    { status, code, reason }: NetworkChangeContext
  ) => {
    // If the network change was a disconnect
    if (status === NetworkStatus.Disconnected) {
      // Load the route context with the reason sent by the websocket
      setContext({
        status,
        code,
        reason,
      });

      // Redirect to login page
      setRoute(Route.Login);
    }
  };

  useEffect(() => {
    // Listen for general route change messages
    const token = PubSub.subscribe(
      'route.change',
      (_: string, { route }: { route: Route }) => {
        console.log('Route change detected');
        setRoute(route);
      }
    );

    // There are some things we want to do when network status changes
    const netstatToken = PubSub.subscribe(
      'network.status',
      handleNetworkStatus
    );

    return () => {
      PubSub.unsubscribe(token);
      PubSub.unsubscribe(netstatToken);
    };
  }, [handleNetworkStatus]);

  return (
    <RouteContext.Provider value={{ route, setRoute, ctx }}>
      {children}
    </RouteContext.Provider>
  );
};
